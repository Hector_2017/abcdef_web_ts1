<html>
  <head>
    <title>ts1</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/style_menu.css">
    <script src="./js/index.js"></script>
  </head>

  <body class="body_c">
    <?php
      //insertamos el menu de opciones
      include "./menu.php";
      session_start();
      $nom = $_SESSION["nombre"];
      $tipo = $_SESSION["tipo"];
      $id = $_SESSION['user'];

      if($nom != "" && $tipo == 2) {
    ?>
        <br><br><br><br><br><br>
        <form method="post" action="">
            <h3> Estas seguro de eliminar tu cuenta  <?php echo $_SESSION["nombre"] . ' ' . $_SESSION["apellido"]; ?>  ??</h3><br><br>
            <button name="eliminar" class="button-submit" >Eliminar</button>
            <button name="cerrar" class="button-submit" >Solo cerrar sesion</button>
            <button name="modificar" class="button-submit">Modificar datos</button>
        </form>       
    
        <?php
            if (isset($_POST['eliminar'])) {
                //include 'config.php';
                include "../php_bd/conexion.php";
                
                $sql = "DELETE FROM Docente WHERE id ='". $id ."'";

                if ($conn->query($sql) === TRUE) {
                    if (ini_get("session.use_cookies")) {
                        $params = session_get_cookie_params();
                        setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                        );
                    }
                    session_destroy();
                    header("Location: ../php/index.php");
                } else {
                    echo "<h4 style='color:#FBC2522'> ha ocurrido un error en la eliminacion de su cuenta!! intente de nuevo!</h4>";
                }                         
                $conn->close();                                     
            }

            if(isset($_POST['modificar'])) {
                header("Location: ./modificar.php");
            }

        ?>
    <?php  
      } else {
        header("Location: ../php/index.php");
      }
    ?>
  </body>

</html>
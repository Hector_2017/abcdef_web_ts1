<html>

<head>
    <title>registro usuario</title>
    <link rel="stylesheet" type="text/css" href="../css/style_registro.css">
    <script src="../js/index.js"></script>
</head>

<body class="body_c">
    <center>
        
        <div class="hijo2">
        <a href="./index.php">regresar</a>
            <!--formulario regisro-->

            <form method="post" action="">
                <h3><?php print " REGISTRO NUEVO USUARIO"  ?> </h3><br><br>
                <label class="label-dat" for="user">seleccione un tipo de usuario:<span style="color:red">*</span></label>
                <select class="combo" id="tipo" name="tipo">
                    <option value="1">Estudiante</option>
                    <option value="2">Docente</option>
                    <option value="3">Administrador</option>
                </select><br><br><br>
                <label class="label-dat"> Codigo de usuario:<span style="color:red">*</span></label>
                <input type="number" min="1000" max="10000000" name="user" class="input-dat" placeholder="codigo para iniciar sesion" required />
                <label>minimos de 4 digitos y maximo de 8 <span style="color:red">*</span></label>
                <!--<input type="text" name="name" class="input-dat" disabled required value="1234">-->
                <br><br>
                <label class="label-dat"> Name:<span style="color:red">*</span></label>
                <input type="text" name="name" class="input-dat" placeholder="nombres" required />
                <br><br>
                <label class="label-dat"> Apellido:<span style="color:red">*</span> </label>
                <input type="text" name="apellido" class="input-dat" placeholder="apellidos" required />
                <br><br>
                <label class="label-dat"> Telefono:<span style="color:red">*</span></label>
                <input type="number" min="10000000" max="10000000000" name="tel" class="input-dat" placeholder="numero telefono" required />
                <br><br>
                <label class="label-dat"> E-mail:<span style="color:red">*</span></label>
                <input type="text" name="email" class="input-dat" placeholder="correo electronico" required />
                <br><br>
                <label class="label-dat"> Password:<span style="color:red">*</span></label>
                <input type="password" placeholder="password (minimo 5 carcteres)" minlength="5" class="input-dat" name="password" id="password" required />
                <button class="button-submit" type="button" onclick="mostrarContrasena();">
                    ver/ocultar</button><br>
                <h6></h6>
                <br><br>
                <label class="label-dat"> GENERO:<span style="color:red">*</span></label>
                <select class="label-dat" id="genero" name="genero">
                    <option value="Mujer">Mujer</option>
                    <option value="Hombre">Hombre</option>
                </select><br><br><br>
                <button name="registrar" class="button-submit" >Registrar usuario</button><br>
            </form>


        <?php
            if (isset($_POST['registrar'])) {
                session_start();
                //include 'config.php';
                include "../php_bd/conexion.php";

                $pass = $id = $tipo = $name = $apellido = $tel = $email = $genero = "";
                // collect value of input field
                $id = $_POST['user'];
                $pass = md5($_POST['password']);
                $tipo = $_POST['tipo'];
                $name = $_POST['name'];
                $apellido = $_POST['apellido'];
                $tel = $_POST['tel'];
                $email = $_POST['email'];
                $genero = $_POST['genero'];

                // sentencia para el tipo de usuario seleccionado
                if ($tipo == 1) {
                    $sql = "SELECT * FROM Alumno WHERE id ='" . $id . "'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) { 
                        echo "<h4 style='color:#FF0000'> el id ingresado ya existe, cambie e intente de nuevo!</h4>";
                    } else {
                        $sql = "INSERT INTO Alumno (id, nombre, apellido, pasword, telefono, email, genero)
                        VALUES (' ". $id ."', '". $name ."', '". $apellido ."', '". $pass ."', '". $tel ."', '". $email ."', '". $genero ."')";

                        if ($conn->query($sql) === TRUE) {
                            session_start();
                            $_SESSION["user"]= $id;
                            $_SESSION["nombre"]= $name;
                            $_SESSION["apellido"]= $apellido;
                            $_SESSION["email"]= $email;
                            $_SESSION["tipo"]= $tipo;
                            header("Location: ./registro.php");
                        } else {
                            echo "<h4 style='color:#FBC2522'> ha ocurrido un error en el registro, 
                            id ya existe, intente con un nuevo codigo usuario(id)!</h4>";
                        } 
                    }
                    
                    $conn->close();
                    
                } else if ($tipo == 2) {
                    $sql = "SELECT * FROM Docente WHERE id ='" . $id . "'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) { 
                        echo "<h4 style='color:#FF0000'> el id ingresado ya existe, cambie e intente de nuevo!</h4>";
                    } else {
                        $sql = "INSERT INTO Docente (id, nombre, apellido, pasword, telefono, email, genero)
                        VALUES (' ". $id ."', '". $name ."', '". $apellido ."', '". $pass ."', '". $tel ."', '". $email ."', '". $genero ."')";

                        if ($conn->query($sql) === TRUE) {
                            session_start();
                            $_SESSION["user"]= $id;
                            $_SESSION["nombre"]= $name;
                            $_SESSION["apellido"]= $apellido;
                            $_SESSION["email"]= $email;
                            $_SESSION["tipo"]= $tipo;
                            header("Location: ./registro.php");
                        } else {
                            echo "<h4 style='color:#FBC2522'> ha ocurrido un error en el registro, 
                            id ya existe, intente con un nuevo codigo usuario(id)!</h4>";
                        } 
                    }
                    
                    $conn->close();

                } else {
                    $sql = "SELECT * FROM Administrador WHERE id ='" . $id . "'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) { 
                        echo "<h4 style='color:#FF0000'> el id ingresado ya existe, cambie e intente de nuevo!</h4>";
                    } else {
                        $sql = "INSERT INTO Administrador (id, nombre, apellido, pasword, telefono, email, genero)
                        VALUES (' ". $id ."', '". $name ."', '". $apellido ."', '". $pass ."', '". $tel ."', '". $email ."', '". $genero ."')";

                        if ($conn->query($sql) === TRUE) {
                            session_start();
                            $_SESSION["user"]= $id;
                            $_SESSION["nombre"]= $name;
                            $_SESSION["apellido"]= $apellido;
                            $_SESSION["email"]= $email;
                            $_SESSION["tipo"]= $tipo;
                            header("Location: ./registro.php");
                        } else {
                            echo "<h4 style='color:#FBC2522'> ha ocurrido un error en el registro, 
                            id ya existe, intente con un nuevo codigo usuario(id)!</h4>";
                        } 
                    }
                    
                    $conn->close();
                }
            }

        ?>

        </div>
    </center>

</body>

</html>
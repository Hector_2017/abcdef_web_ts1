<!DOCTYPE html>
<html>
    <head>
        <title>login ts</title>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script src="../js/index.js"></script>
    </head>
    <body>
        
        <div class="login-page">
            <div class="form">
                <h1>TS1 CUNOC</h1>
                
                <form class="login-form" method="post">
                    <label for="user">seleccione un tipo de usuario:<span style="color:red">*</span></label>
                    <select class="label-dat" id="tipo" name="tipo">
                        <option value="1">Estudiante</option>
                        <option value="2">Docente</option>
                        <option value="3">Administrador</option>
                    </select>
                    <input type="number" min="1000" max="10000000" placeholder="codigo_usuario" name="user" id="user" required />
                    <input type="password" placeholder="password" name="password" id="password" required />
                    <button class="button-submit" type="button" onclick="mostrarContrasena();">
                        ver/ocultar</button><br><h6></h6>
                    <button name="enviar" class="button-login">Iniciar sesion</button><br><br><h6></h6>
                    <p class="message">aun no tienes cuenta? <a href="./registrar.php">Crear una
                    cuenta</a></p><br>
                    <?php
                        if (isset($_POST['enviar'])) {
                            session_start();
                            //include 'config.php';
                            include "../php_bd/conexion.php";

                            $pass = $user = $tipo ="";
                            // collect value of input field
                            $user = $_POST['user'];
                            $pass = md5($_POST['password']);
                            $tipo = $_POST['tipo'];

                            if($tipo == 1) {
                                $sql = "SELECT * FROM Alumno WHERE id ='" . $user . "' AND pasword ='" . $pass . "'";
                                //$sql = 'SELECT id, nombre, apellido FROM Alumno';
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        $_SESSION["user"]= $row["id"];
                                        $_SESSION["nombre"]= $row["nombre"];
                                        $_SESSION["apellido"]= $row["apellido"];
                                        $_SESSION["email"]= $row["email"];
                                        $_SESSION["tipo"]= $tipo;
                                        header("Location: ../estudiante_php/inicio.php");
                                    }
                                } else {
                                    echo "<h4 style='color:#FF0000'>codigo de usuario o Password Incorrectos,\n intente de nuevo!!</h4>";
                                    echo "<br>";
                                } 
                            } else if($tipo == 2) {
                                $sql = "SELECT * FROM Docente WHERE id ='" . $user . "' AND pasword ='" . $pass . "'";
                                $result = $conn->query($sql);
                                
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        
                                        $_SESSION["user"]= $row["id"];
                                        $_SESSION["nombre"]= $row["nombre"];
                                        $_SESSION["apellido"]= $row["apellido"];
                                        $_SESSION["email"]= $row["email"];
                                        $_SESSION["tipo"]= $tipo;
                                        header("Location: ../docente_php/inicio.php");
                                    }
                                } else {
                                    echo "<h4 style='color:#FF0000'>codigo de usuario o Password Incorrectos, intente de nuevo!!</h4>";
                                    echo "<br>";
                                }
                            } else {
                                $sql = "SELECT * FROM Administrador WHERE id ='" . $user . "' AND pasword ='" . $pass . "'";
                                $result = $conn->query($sql);
                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        
                                        $_SESSION["user"]= $row["id"];
                                        $_SESSION["nombre"]= $row["nombre"];
                                        $_SESSION["apellido"]= $row["apellido"];
                                        $_SESSION["email"]= $row["email"];
                                        $_SESSION["tipo"]= $tipo;
                                        header("Location: ../admin_php/inicio.php");
                                    }
                                } else {
                                    echo "<h4 style='color:#FF0000'>codigo de usuario o Password Incorrectos, intente de nuevo!!</h4>";
                                    echo "<br>";
                                }
                            }
                        }
        
                    ?>
                </form>
            </div>
        </div> 
    </body>
</html>
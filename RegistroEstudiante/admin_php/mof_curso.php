<html>
  <head>
    <title>ts1</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/style_menu.css">
    <script src="./js/index.js"></script>
  </head>

  <body class="body_c">
    <?php
      //insertamos el menu de opciones
        include "./menu.php";
        session_start();
        $nom = $_SESSION["nombre"];
        $tipo = $_SESSION["tipo"];

        if($nom != "" && $tipo == 3) {
    ?>
    <br><br><br><br><br><br>
        <?php      
            include "../php_bd/conexion.php";
            $sql = "SELECT * FROM Curso";
            $result = $conn->query($sql);

            if ($result->num_rows <= 0) { 
                echo "<h4 style='color:#FF0000'> No existen cursos para mostrar!</h4>";
            } else {
                echo "<h2 style='color:#FF0000'> listado de cursos </h2>"
        ?>
                <table border="7" width="1000"align ="center" >
                    <tr>
                        <th>codigo curso</th>
                        <th>nombre</th>
                        <th>creditos</th>
                        <th>descripcion</th>
                        <th>id docente</th>
                        <th>nombre docente</th>
                    </tr>
        <?php
                 // output data of each row
                 while($row = $result->fetch_assoc()) {
                     $docente = $row["docente_id"];

        ?>               
                    <tr>
                        <td align="center"> <?php echo $row["codigo"]; ?> </td>
                        <td align="center"> <?php echo $row["nombre"]; ?> </td>
                        <td align="center"> <?php echo $row["creditos"]; ?> </td>
                        <td align="center"> <?php echo $row["descripcion"]; ?> </td>
                        <td align="center"> <?php echo $docente; ?> </td>

                        <td align="center"> <?php
                         $sql1 = "SELECT nombre, apellido FROM Docente WHERE id ='$docente'";
                         $result1 = $conn->query($sql1);
                         $nom = $apell = "";
                         if ($result1->num_rows > 0) {
                             // output data of each row
                             while($row = $result1->fetch_assoc()) {
                                 
                                 $nom= $row["nombre"];
                                 $apell= $row["apellido"];
                             }
                         }
                        echo $nom . " " . $apell; ?> </td>
                        
                    </tr>            
        <?php 
                }
                ?>
                </table>
                <?php 
            }
                    
            $conn->close();                
     
        } else {
            header("Location: ../php/index.php");
        }
    ?>
  </body>

</html>
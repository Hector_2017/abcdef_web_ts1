<html>
    <head>
        <title>nuevo usuario</title>
        <link rel="stylesheet" type="text/css" href="../css/style_registro.css">
    </head>
    <body>
        <?php
            session_start();
            
        ?>

        <div class="div"><br><br>
            <div>
                <h2 class="label-dat">Se creo el usuario correctamente..</h2>
            </div>
            <form method="post">
                <h3 class="button-submit">Selecciona una opcion:</h3>
                <button class="label" name="index" id="index">ir al login</button>
                <button class="label" name="cuenta" id="cuenta">ir a mi cuenta</button>
                <button class="label" name="crear" id="crear">registrar nuevo usuario</button>
            </form>
        </div>
        <?php
            
            if (isset($_POST['index'])) {
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                    );
                }
               // <a href="../php/index.php">cerrar sesion</a>
                // Finalmente, destruir la sesión.
                session_destroy();
                header("Location: ./index.php"); 
            }

            if (isset($_POST['crear'])) {
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                    );
                }
               // <a href="../php/index.php">cerrar sesion</a>
                // Finalmente, destruir la sesión.
                session_destroy();
                header("Location: ./registrar.php"); 
            }

            if (isset($_POST['cuenta'])) {
                session_start();
                $nom = $_SESSION["nombre"];
                $tipo = $_SESSION["tipo"];
                if($nom != "") {
                    if($tipo == 1) {
                        header("Location: ../estudiante_php/inicio.php");
                    } else if($tipo == 2) {
                        header("Location: ../docente_php/inicio.php");
                    } else {
                        header("Location: ../admin_php/inicio.php");
                    }

                } else {    
                    if (ini_get("session.use_cookies")) {
                        $params = session_get_cookie_params();
                        setcookie(session_name(), '', time() - 42000,
                            $params["path"], $params["domain"],
                            $params["secure"], $params["httponly"]
                        );
                    }
                   // <a href="../php/index.php">cerrar sesion</a>
                    // Finalmente, destruir la sesión.
                    session_destroy();
                    header("Location: ./registrar.php");
                }
            }
            
        ?>
    </body>
</html>
<html>
  <head>
    <title>ts1</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/style_menu.css">
    <script src="./js/index.js"></script>
  </head>

  <body class="body_c">
    <?php
      //insertamos el menu de opciones
        include "./menu.php";
        session_start();
        $nom = $_SESSION["nombre"];
        $tipo = $_SESSION["tipo"];
        $id = $_SESSION['user'];

        if($nom != "" && $tipo == 1) {
    ?>
    <br><br><br><br><br><br>
        <?php      
            include "../php_bd/conexion.php";
            $sql = "SELECT * FROM Notas WHERE alumno_id = '$id'";
            $result = $conn->query($sql);

            if ($result->num_rows <= 0) { 
                echo "<h4 style='color:#FF0000'> No existen notas para mostrar!</h4>";
            } else {
                
                echo "<h2 style='color:#FF0000'> listado de Notas </h2>"
        ?>
                <table border="7" width="1000"align ="center" >
                    <tr>
                        <th>codigo curso</th>
                        <th>nombre curso</th>
                        <th>id docente</th>
                        <th>zona</th>
                        <th>final</th>
                        <th>total</th>
                        <th>estatus</th>
                    </tr>
        <?php
                 // output data of each row
                 while($row = $result->fetch_assoc()) {
                     $docente = $row["docente_id"];
                     $curso = $row["curos_id"];
                     $zona = $row["zona"];
                     $final = $row["final"];
                     $total = $row["total"];
                     $estado = $row["estado"];
                     $nomcurso = "";

                    $sql1 = "SELECT nombre FROM Curso WHERE codigo ='$curso'";
                    $result1 = $conn->query($sql1);
                         
                         if ($result1->num_rows > 0) {
                             // output data of each row
                             while($row = $result1->fetch_assoc()) {
                                 
                                 $nomcurso= $row["nombre"];
                             }
                         }
                    if($estado == 'aprobado') {

        ?>               
                    <tr>
                        <td align="center"> <?php echo $curso; ?> </td>
                        <td align="center"> <?php echo $nomcurso; ?> </td>
                        <td align="center"> <?php echo $docente; ?> </td>
                        <td align="center"> <?php echo $zona; ?> </td>
                        <td align="center"> <?php echo $final; ?> </td>
                        <td align="center"> <?php echo $total; ?> </td>
                        <td align="center"> <?php echo $estado; ?> </td>
                        
                    </tr>            
        <?php        } else {
                        ?>               
                        <tr style="background-color: red;">
                            <td align="center"> <?php echo $curso; ?> </td>
                            <td align="center"> <?php echo $nomcurso; ?> </td>
                            <td align="center"> <?php echo $docente; ?> </td>
                            <td align="center"> <?php echo $zona; ?> </td>
                            <td align="center"> <?php echo $final; ?> </td>
                            <td align="center"> <?php echo $total; ?> </td>
                            <td align="center"> <?php echo $estado; ?> </td>
                            
                        </tr>            
            <?php 
                    }
                }
                ?>
                </table>
                <?php 
            }
                    
            $conn->close();                
     
        } else {
            header("Location: ../php/index.php");
        }
    ?>
  </body>

</html>
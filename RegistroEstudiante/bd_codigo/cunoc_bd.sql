/*
* autor: @hectoradolfo
* base de datos
*/
CREATE SCHEMA cunoc_bd;

USE cunoc_bd;

CREATE TABLE Administrador(
    id INT NOT NULL,
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,
    pasword VARCHAR(45) NOT NULL,
    telefono VARCHAR(13),
    email VARCHAR(90) NOT NULL,
    genero VARCHAR(6) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Alumno(
    id INT NOT NULL,
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,
    pasword VARCHAR(45) NOT NULL,
    telefono VARCHAR(13),
    email VARCHAR(90) NOT NULL,
    genero VARCHAR(6) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Docente(
    id INT NOT NULL,
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,
    pasword VARCHAR(45) NOT NULL,
    telefono VARCHAR(13),
    email VARCHAR(90) NOT NULL,
    genero VARCHAR(6) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Curso(
    codigo INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    creditos INT NOT NULL,
    docente_id INT,
    descripcion TEXT,
    PRIMARY KEY(codigo),
    FOREIGN KEY (docente_id) REFERENCES Docente(id)
);

CREATE TABLE Notas(
    id INT NOT NULL AUTO_INCREMENT,
    alumno_id INT NOT NULL,
    docente_id INT NOT NULL,
    curos_id INT NOT NULL,
    zona INT NOT NULL,
    final INT NOT NULL,
    total INT NOT NULL,
    estado VARCHAR(10),
    PRIMARY KEY(id),
    FOREIGN KEY (alumno_id) REFERENCES Alumno(id),
    FOREIGN KEY (docente_id) REFERENCES Docente(id),
    FOREIGN KEY (curos_id) REFERENCES Curso(codigo)
);
<html>
<head>
    <title>modificar usuario</title>
    <link rel="stylesheet" type="text/css" href="../css/style_registro.css">
    <script src="../js/index.js"></script>
</head>

<body class="body_c">
    <?php
        //insertamos el menu de opciones
        include "./menu.php";
        session_start();
        include "../php_bd/conexion.php";
        $nom = $_SESSION["nombre"];
        $tipo = $_SESSION["tipo"];
        $id = $_SESSION['user'];

        if($nom != "" && $tipo == 3) {
    ?>
    <br><br><br>
    <center>
        <div class="hijo2">
            <!--formulario regisro-->
            <form method="post" action="">
                <h3><?php print " Crear nevo curso "  ?> </h3><br><br>
                
                <label class="label-dat"> Codigo de curso:<span style="color:red">*</span></label>
                <input type="number" min="10" max="10000000" name="codigo" class="input-dat" placeholder="codigo curso" required />
                <label>minimos de 4 digitos y maximo de 8 <span style="color:red">*</span></label>
                <!--<input type="text" name="name" class="input-dat" disabled required value="1234">-->
                <br><br>
                <label class="label-dat"> Nombre curso:<span style="color:red">*</span></label>
                <input type="text" name="nombre" class="input-dat" placeholder="nombre curso" required />
                <br><br>
                <label class="label-dat"> descripcion:<span style="color:red">*</span> </label>
                <textarea rows="5" name="despcripcion" class="input-dat" placeholder="descripcion del curso" required ></textarea>
                <br><br>
                <label class="label-dat"> Creditos:<span style="color:red">*</span></label>
                <input type="number" min="0" max="20" name="creditos" class="input-dat" placeholder="numero de credito" required />
                <br><br>
                <label class="label-dat"> Docente que impartira el curso:<span style="color:red">*</span></label>
                <input type="number" min="1000" max="10000000" name="docente" class="input-dat" placeholder="id del docente" required />
                <br><br><br>
                <button name="crear" class="button-submit" >Crear curso</button><br><br>
            </form>


        <?php
            if (isset($_POST['crear'])) {
                //include 'config.php';
                include "../php_bd/conexion.php";
                // collect value of input field
                $codigo = $_POST['codigo'];
                $nombre = $_POST['nombre'];
                $descripcion = $_POST['despcripcion'];
                $creditos = $_POST['creditos'];
                $docente = $_POST['docente'];

                // sentencia para el tipo de usuario seleccionado
                $sql = "SELECT nombre FROM Docente WHERE id ='$docente'";
                $result = $conn->query($sql);

                if ($result->num_rows <= 0) { 
                    echo "<h4 style='color:#FF0000'> no existe un docente con el codigo '$docente'!</h4>";
                } else {
                    $sql = "INSERT INTO Curso (codigo, nombre, creditos, docente_id, descripcion)
                    VALUES ('$codigo', '$nombre', '$creditos', '$docente', '$descripcion')";

                    if ($conn->query($sql) === TRUE) {
                        session_start();
                        echo "<h4 style='color:#06680B'> se ha registrado un nuevo curso!</h4>";                            
                    } else {
                        echo "<h4 style='color:#FF5722'> ha ocurrido un error en el registro, 
                        ya existe un curso con ese codigo, intente con un nuevo codigo(id)!</h4>";
                    } 
                }
                    
                $conn->close();                  
                 
            }

        ?>

        </div>
    </center>
    <?php  
        } else {
            header("Location: ../php/index.php");
        }
    ?>            
</body>

</html>
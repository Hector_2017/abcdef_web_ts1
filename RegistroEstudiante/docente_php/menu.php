<!DOCTYPE html>
<html>
    <head>
        
        <title>menu</title>
        <link rel="stylesheet" href="../css/style_menu.css">
    </head>
    <body>
        <header>
            <nav>
                <ul id="menu">
                    <li><a href="./inicio.php">Inicio</a></li>
                    <li><a href="">Mis datos</a>
                        <ul>
                            <li><a href="./modificar.php">Modifiar</a></li>
                            <li><a href="./eliminar.php">Eliminar</a></li>                         
                        </ul>
                    </li>
                    <li><a href="">Reportes</a>
                        <ul>
                            <li><a href="../RegistroEstudiante/php/index.html">Notas</a></li>
                            <li><a href="./curso.php">Cursos</a></li>
                        </ul>
                    </li>    
                    <li><a href="">Creditos</a>
                        <ul>
                            <li><a href="http://localhost:60/Practica_TS_2021/main.php">Contacto</a></li>
                            <li><a href="">Ayuda</a></li>
                        </ul>
                    </li>
                    <li><a href="">sesion</a>
                        <ul>
                            <li><form class="login-form" method="post" action=""> <button class="label-dat" name="cerrar" >Cerrar sesion</button></form></li>                            
                        </ul>
                        
                    </li>
                </ul>
            </nav>
        </header>
        <?php
                // procedimiento para cerrar sesion
                if (isset($_POST['cerrar'])) {
                    if (ini_get("session.use_cookies")) {
                        $params = session_get_cookie_params();
                        setcookie(session_name(), '', time() - 42000,
                        $params["path"], $params["domain"],
                        $params["secure"], $params["httponly"]
                        );
                    }
                    session_destroy();
                    header("Location: ../php/index.php"); 
                }                    
        ?>
    </body>
</html>